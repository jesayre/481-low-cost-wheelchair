
#include <NewPing.h>

#define MAX_DISTANCE 100              
#define MAX_SPEED 30                  // Max wheel speed for going forward & reverse
#define MAX_SPEED_TURN 50             // Max wheel speed for turning

const int TRIG_PIN[] = {51, 49, 47, 45, 43};      // Green wire, pins for US sensors
const int ECHO_PIN[] = {50, 48, 46, 44, 42};     // Red wire, pins for US sensors
const int INDICATOR[] = {41, 40, 39, 38, 37};    // Pins for US sensors

int median_window = 3;
bool objDetectStatus[] = {false, false, false, false, false};  // {front, left, right, left back, right back} US Sensors
float stopDistance_cm = 60.0;         // US Sensors distance
float goDistance_cm = 68.0;           // Us Sensors distance
float duration = 0;
float distance = 0;

NewPing US0(TRIG_PIN[0], ECHO_PIN[0], MAX_DISTANCE);  //Forward
NewPing US1(TRIG_PIN[1], ECHO_PIN[1], MAX_DISTANCE);  //Left
NewPing US2(TRIG_PIN[2], ECHO_PIN[2], MAX_DISTANCE);  //Right
NewPing US3(TRIG_PIN[3], ECHO_PIN[3], MAX_DISTANCE);  //Back Left
NewPing US4(TRIG_PIN[4], ECHO_PIN[4], MAX_DISTANCE);  //Back Right

int US_num = 0;

//Joystick
const int analogJoysticX = A0;  // Analog input pin that the joystick X direction is attached to
const int analogJoysticY = A1;  // Analog input pin that the joystick X direction is attached to
int xValue = 0;
int yValue = 0;

//Motor Speed
const int motor_speedRight = A2;
const int motor_speedLeft = A3;
int meas_speedRight = 0;
int meas_speedLeft = 0;

//Buttons
const int buttonA = 2;
const int buttonB = 3;
const int buttonC = 4;
const int buttonD = 5;
const int buttonE = 6;
const int buttonF = 7;
const int buttonK = 8;

// Wheel Speed Control
const int rightWheelSpeed_PWM = 10;
const int leftWheelSpeed_PWM = 9;

//Motion IR Sensors Hand Control
const int botIR = 23;
const int botRightIR = 24;
const int topRightIR = 25;
const int frontIR = 26;
const int leftIR = 27;

//Diagnostic LEDs
const int LED_sensors = 28; // White
const int LED_object = 29; // Red
const int LED_IR = 30; // Orange
const int LED_joystick = 31; //Green

//Wheel Control
const int pin_directionControlRight = 32;
const int pin_directionControlLeft = 33;
const int toggle_driverRight = 34;  //LOW-OFF HIGH-ON
const int toggle_driverLeft = 35;  //LOW-OFF HIGH-ON

//Buttons Setup
int buttonStateA = HIGH;
int buttonStateB = HIGH;
int buttonStateC = HIGH;
int buttonStateD = HIGH;
int buttonStateE = HIGH;
int buttonStateF = HIGH;
int buttonStateK = HIGH;
int lastButtonStateA = HIGH;
int lastButtonStateB = HIGH;
int lastButtonStateC = HIGH;
int lastButtonStateD = HIGH;
int lastButtonStateE = HIGH;
int lastButtonStateF = HIGH;
int lastButtonStateK = HIGH;

const int Stationary = 0;
const int Forward = 1;
const int Reverse = 2;

int LastMove = Stationary;
//IR Sensors Cont
int topRightStateIR = HIGH;
int leftStateIR = HIGH;
int frontStateIR = HIGH;
int botRightStateIR = HIGH;
int botStateIR = HIGH;



int directionControlLeft = LOW;
int directionControlRight = LOW;

int zVector[4] = {0,0,0,0};

boolean zoneControl = true;
boolean readIRSensors = false;
boolean sensorsDisabled = false;

boolean standby1 = false;
boolean standby2 = false;
boolean delayStart = false;

boolean set = false;
boolean standby = false;
boolean turnRightS = false;
boolean turnLeftS = false;
boolean reverseS = false;
boolean forwardS = false;

unsigned long int startTime;
unsigned long int currentTime;


float Read_US_MED(int num) //alternative to object vectorization
{ 
  /*
   Read_US_MED (read ultrasonic sensor median) method to easily determine the distance
   read from an ultrasonic sensor. The output of the function is the median distance in cm
   of several readings from a given sensor.  Note the max number of sensors.
   Required Library: NewPing.h
   Required Methods: NewPing.ping_median(windowlength)
   Required Variables: duration, median_window, distance.
  */
  switch (num)
  {
    case 0:
      duration = US0.ping_median(median_window);
      break;

    case 1:
      duration = US1.ping_median(median_window);
      break;

    case 2:
      duration = US2.ping_median(median_window);
      break;

    case 3:
      duration = US3.ping_median(median_window);
      break;

    case 4:
      duration = US4.ping_median(median_window);
      break;

    default:
      return -1;
      break;
  }
  if (duration < 1) distance = -1; //-1 marks an 'OUT OF RANGE' reading
  else distance = (duration / 2) * 0.0343;
  return distance;
}

void Ultrasonic_Array_Test(int USindex)
{ 
  /*
   Ultrasonic_Array is a method used  to manage an array of ultrasonic sensors
   The method acts as a hysteresis filter for object detection
   Required Library: NewPing.h
   Required Methods: Read_US_MED(int num)
   Required Variables: objDetectStatus[int USnumb], stopDistance_cm, goDistance_cm
   INDICATOR[int USnumb],
  */
  int distance_cm;
  
    distance_cm = Read_US_MED(USindex);
    if ((distance_cm == -1) || ((objDetectStatus[USindex]) && (distance_cm > goDistance_cm)))
    {
      objDetectStatus[USindex] = false;
            digitalWrite(INDICATOR[USindex], HIGH);
    }
    else if (distance_cm <= stopDistance_cm)
    {
      objDetectStatus[USindex] = true;
            digitalWrite(INDICATOR[USindex], LOW);
            Serial.print("Object Detected\n");
    }
}

void setup() {
  
  /* stop and disable drivers for safety*/
  movement_stop();
  disable_drivers();

  // Initialize pushbuttons
  pinMode(buttonA, INPUT);
  pinMode(buttonB, INPUT);
  pinMode(buttonC, INPUT);
  pinMode(buttonD, INPUT);
  pinMode(buttonE, INPUT);
  pinMode(buttonF, INPUT);
  pinMode(buttonK, INPUT);
  
  //Initialize IR
  pinMode(topRightIR, INPUT);
  pinMode(leftIR, INPUT);
  pinMode(botIR, INPUT);
  pinMode(botRightIR, INPUT);
  pinMode(frontIR, INPUT);
  
  // INitialize LEDs
  pinMode(LED_sensors, OUTPUT);
  pinMode(LED_object, OUTPUT);
  pinMode(LED_IR, OUTPUT);
  pinMode(LED_joystick, OUTPUT);
  
  // Initialize Drivers
  pinMode(pin_directionControlRight, OUTPUT);
  pinMode(pin_directionControlLeft, OUTPUT);
  pinMode(toggle_driverRight, OUTPUT);
  pinMode(toggle_driverLeft, OUTPUT);
  pinMode(rightWheelSpeed_PWM, OUTPUT);
  pinMode(leftWheelSpeed_PWM, OUTPUT);
  
  
  digitalWrite(LED_joystick, LOW);
  digitalWrite(LED_IR, HIGH);
  digitalWrite(LED_sensors, LOW);
  digitalWrite(LED_object, HIGH);


  for (int i = 0; i < 5; i++)
  {
    pinMode(INDICATOR[i], OUTPUT);
    digitalWrite(INDICATOR[i], HIGH);
  }
  
  // initialize serial comm at 9600 bps
  Serial.begin(9600);
}

// Main loop, where everything is executed
void loop(){    
  setupStuff();
  checkButtonStates();
  checkZoneControl();
  checkSensors(sensorsDisabled);
}

// Setup
void setupStuff(){
  xValue = analogRead(analogJoysticX);
  yValue = analogRead(analogJoysticY);
  meas_speedRight = analogRead(motor_speedRight);
  meas_speedLeft = analogRead(motor_speedLeft);
  buttonStateA = digitalRead(buttonA);
  buttonStateB = digitalRead(buttonB);
  buttonStateC = digitalRead(buttonC);
  buttonStateD = digitalRead(buttonD);
  buttonStateE = digitalRead(buttonE);
  buttonStateF = digitalRead(buttonF);
  buttonStateK = digitalRead(buttonK);
  topRightStateIR = digitalRead(topRightIR);
  leftStateIR = digitalRead(leftIR);
  frontStateIR = digitalRead(frontIR);
  botRightStateIR = digitalRead(botRightIR);
  botStateIR = digitalRead(botIR);
}

// Reads the zone the joystick is pointed in, moves in the direction of the joystick if the joystick/movement is enabled
void checkZoneControl(){
  //zone control (enabled if zone control true)
  if (zoneControl == true){
    for (int i = 3;i>0;i--){
      zVector[i] = zVector[i-1];
    }
    zVector[0] = readJoystickZone(xValue, yValue);
      if (zVector[0] == zVector[1] && zVector[2] == zVector[3] && zVector[1] == zVector[2] ){
        if (zVector[0]!=0){
          Serial.println(zVector[0]); // any index of zVector contains the zone of operation used for direction control
        }
        enable_drivers();
        switch (zVector[0])
  {
    case 0:
      if (LastMove==Forward){
        movement_stop_fromForward();
        LastMove=Stationary;
      }
      if (LastMove==Reverse){
        movement_stop_fromReverse();
        LastMove=Stationary;
      }
      movement_stop();
      break;
      
    case 1:
      turnRight();
      break;
      
    case 2:
      moveForward();
      break;

    case 3:
      turnLeft();
      break;

    case 4:
      moveReverse();
      break;
  }
      }
    }
      else if(readIRSensors == true){
       //readZoneIR(frontStateIR, topRightStateIR, botRightStateIR, botStateIR, leftStateIR);
       changeIR(frontStateIR, topRightStateIR, botRightStateIR, botStateIR, leftStateIR);
    }
    else{
      movement_stop();
      disable_drivers();
    }
} 


void checkButtonStates(){
/*
  pressed == LOW, then check to see if released
   Checks to see if any buttons have been pressed and executes when button is pressed
   Button A: Swap between Joystick and IR sensors for movement
   Button B: Enable/Disable movement 
   Button C: Not used
   Button D: Enable/Disable US sensors
*/

  if (buttonStateA == HIGH && lastButtonStateA == LOW) {  // TRIGGER IR SENSORS
   readIRSensors = !readIRSensors;
   if(readIRSensors == true){
     zoneControl = false;
     Serial.print("Joystick Disabled, IR Enabled \n");
     digitalWrite(LED_joystick, HIGH);
     digitalWrite(LED_IR, LOW);
   }
   else{
     zoneControl = true;
     Serial.print("Joystick Enbled, IR Disabled \n");
     digitalWrite(LED_joystick, LOW);
     digitalWrite(LED_IR, HIGH);
   }
  }
  
  if (buttonStateB == HIGH && lastButtonStateB == LOW) {  // DISABLE MOVEMENT
    zoneControl = !zoneControl;
    if (zoneControl == true){
      readIRSensors = false;
      Serial.print("Movement Enabled \n");
      digitalWrite(LED_joystick, LOW); //LED ON
      digitalWrite(LED_IR, HIGH);
    }
    else{
      readIRSensors = false;
      Serial.print("Movemen Disabled, IR Disabled \n");
       digitalWrite(LED_joystick, HIGH);
       digitalWrite(LED_IR, HIGH);
    }
  }
  
  if (buttonStateC == HIGH && lastButtonStateC == LOW) {
    Serial.print("Button C Pressed \n");
  }
  
  if (buttonStateD == HIGH && lastButtonStateD == LOW) { // TRIGGER OBJECT SENSORS
    sensorsDisabled = !sensorsDisabled;
    if (sensorsDisabled == false){
      Serial.print("Sensors Enabled \n");
      digitalWrite(LED_sensors, LOW); //LED ON
    }
    else{
      Serial.print("Sensors Disabled \n");
      digitalWrite(LED_sensors, HIGH); //LED OFF
    }
  }
// Not being used  
  if (buttonStateE == HIGH && lastButtonStateE == LOW) {
    Serial.print("Button E Pressed \n");
  }
  
  if (buttonStateF == HIGH && lastButtonStateF == LOW) {
    Serial.print("Button F Pressed \n");
  }
  
  if (buttonStateK == HIGH && lastButtonStateK == LOW) {
    Serial.print("Button K Released \n");
  }

  
  lastButtonStateA = buttonStateA;
  lastButtonStateB = buttonStateB;
  lastButtonStateC = buttonStateC;
  lastButtonStateD = buttonStateD;
  lastButtonStateE = buttonStateE;
  lastButtonStateF = buttonStateF;
  lastButtonStateK = buttonStateK;
}

void cycleSensors(){   //Cycles through the US sensors, checking for an object
  Ultrasonic_Array_Test(US_num);
  US_num++;
  if (US_num>4){
    US_num = 0;
  }
}




void moveForward(){ 
  /* 
     Function for moving the chair forward
     1. Set wheels to spin proper directions
     2. check if sensors enabled
        - If yes, go to checkSensors
        - If no, set lastMove to reverse (braking purposes)and Write the wheels to move
  */
   digitalWrite(pin_directionControlLeft, HIGH);  
   digitalWrite(pin_directionControlRight, LOW);
   if (sensorsDisabled == false){
     checkForward();
   }
   else{
     LastMove=Forward;
     analogWrite(rightWheelSpeed_PWM, MAX_SPEED);
     analogWrite(leftWheelSpeed_PWM, MAX_SPEED);
   }
  }
  
void moveReverse(){      
  /* 
     Function for moving the chair reverse
     1. Set wheels to spin proper directions
     2. check if sensors enabled
        - If yes, go to checkSensors
        - If no, set lastMove to reverse (braking purposes)and Write the wheels to move
  */
   digitalWrite(pin_directionControlLeft, LOW);
   digitalWrite(pin_directionControlRight, HIGH);
   if (sensorsDisabled == false){
     checkReverse();
   }
   else{
     LastMove=Reverse;
     analogWrite(rightWheelSpeed_PWM, MAX_SPEED);
      analogWrite(leftWheelSpeed_PWM, MAX_SPEED);
   }
  }
  
void turnRight(){      
  /* 
     Function for turning the chair right
     1. Set wheels to spin proper directions
     2. check if sensors enabled
        - If yes, go to checkSensors
         If no, Write the wheels to move
  */
   digitalWrite(pin_directionControlLeft, HIGH);
   digitalWrite(pin_directionControlRight, HIGH);
   if (sensorsDisabled == false){
     checkRight();
   }
    else {
      analogWrite(rightWheelSpeed_PWM, MAX_SPEED_TURN);
      analogWrite(leftWheelSpeed_PWM, MAX_SPEED_TURN);
    }
  }
  
void turnLeft(){      
  /* 
     Function for turning the chair left
     1. Set wheels to spin proper directions
     2. check if sensors enabled
        - If yes, go to checkSensors
        - If no, Write the wheels to turn
  */
   digitalWrite(pin_directionControlLeft, LOW);
   digitalWrite(pin_directionControlRight, LOW);
   if (sensorsDisabled == false){
     checkLeft();
   }
     else {
     analogWrite(rightWheelSpeed_PWM, MAX_SPEED_TURN);
     analogWrite(leftWheelSpeed_PWM, MAX_SPEED_TURN);
   }
}
   
  
void movement_stop(){     
  /*
    Sets the speed to 0
  */
   analogWrite(rightWheelSpeed_PWM, 0);
   analogWrite(leftWheelSpeed_PWM, 0);
   digitalWrite(pin_directionControlLeft, LOW);
   digitalWrite(pin_directionControlRight, LOW);
}

// Braking when moving forward 
void movement_stop_fromForward(){
  for (int j=1; j< 21; j++){
  analogWrite(rightWheelSpeed_PWM,80);
  analogWrite(leftWheelSpeed_PWM,80);
  digitalWrite(pin_directionControlLeft, LOW);
  digitalWrite(pin_directionControlRight, HIGH);
  delay(35);
  movement_stop();
  delay(25);
  }
}

// Braking when moving reverse
void movement_stop_fromReverse(){
  for (int j=1; j< 21; j++){
  analogWrite(rightWheelSpeed_PWM,80);
  analogWrite(leftWheelSpeed_PWM, 80);
  digitalWrite(pin_directionControlLeft, HIGH);
  digitalWrite(pin_directionControlRight, LOW);
  delay(35);
  movement_stop();
  delay(25);
  }
}

// disables the motor drivers
void disable_drivers(){
  digitalWrite(toggle_driverRight,LOW);
  digitalWrite(toggle_driverLeft,LOW);
}

// enables the motor drivers
void enable_drivers(){
  digitalWrite(toggle_driverRight,HIGH);
  digitalWrite(toggle_driverLeft,HIGH);
}


void checkForward(){
  /* 
  Checks the US sensors (when enabled) for an object in its path
  Illuminates an LED when an object is detected
  If object enable
  */
  if (objDetectStatus[0] == true) { // object detected
  if (LastMove==Forward){
      movement_stop_fromForward();
      LastMove=Stationary;
  }
       movement_stop();
       digitalWrite(LED_object, LOW); //LED ON
       Serial.print("Object Detected\n");
    }
    else {
      LastMove=Forward;
      digitalWrite(LED_object, HIGH); //LED OFF
      analogWrite(rightWheelSpeed_PWM, MAX_SPEED);
      analogWrite(leftWheelSpeed_PWM, MAX_SPEED);
    }
}

void checkReverse(){
  if (objDetectStatus[3] == true || objDetectStatus[4] == true) { // object detected
    if (LastMove==Reverse){
      movement_stop_fromReverse();
      LastMove=Stationary;
    }
       movement_stop();
       digitalWrite(LED_object, LOW); //LED ON
       Serial.print("Object Detected\n");
    }
    else {
      LastMove=Reverse;
      digitalWrite(LED_object, HIGH); //LED OFF
      analogWrite(rightWheelSpeed_PWM, MAX_SPEED);
      analogWrite(leftWheelSpeed_PWM, MAX_SPEED);
   }
}

void checkRight(){
  if (objDetectStatus[2] == true || objDetectStatus[4] == true) { // object detected
       movement_stop();
       digitalWrite(LED_object, LOW); //LED ON
       Serial.print("Object Detected\n");
    }
    else {
      digitalWrite(LED_object, HIGH); //LED OFF
      analogWrite(rightWheelSpeed_PWM, MAX_SPEED_TURN);
      analogWrite(leftWheelSpeed_PWM, MAX_SPEED_TURN);
    }
}

void checkLeft(){
  if (objDetectStatus[1] == true || objDetectStatus[3] == true) { // object detected
      movement_stop();
      digitalWrite(LED_object, LOW); //LED ON
      Serial.print("Object Detected\n");
    }
    else {
      digitalWrite(LED_object, HIGH); //LED OFF
     analogWrite(rightWheelSpeed_PWM, MAX_SPEED_TURN);
     analogWrite(leftWheelSpeed_PWM, MAX_SPEED_TURN);
  }
}

int readJoystickZone(int xAxis, int yAxis) {
  xAxis = xAxis-512;
  yAxis = yAxis-512;
  int result;
  if (abs(xAxis)+abs(yAxis)<400){
    result = 0; //centered or zone 0
  }
  else if (abs(yAxis)>abs(xAxis)+300){
    if (yAxis>0){
      result = 1;
    }
    else if (yAxis<0){
      result = 3;
    }
  }
  else if (abs(xAxis)>abs(yAxis)+300){
    if (xAxis>0){
      result = 4;
    }
    else if (xAxis<0){
      result = 2;
    }
  }
  else{
    result = 0; //no zone treat as zone 0
  }
  
  return result;
}



int readZoneIR(int front, int topRight, int botRight, int bottom, int left){
  
  if(front == LOW && topRight == HIGH && botRight == HIGH && bottom == HIGH && left == HIGH){
    if(standby == true || forwardS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = false;
      reverseS = false;
      forwardS = true;
     moveForward();
   Serial.print("IR Forward\n");
    }
    else{
     movement_stop();
     standby = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   }
  }
  
  else if(front == HIGH && topRight == LOW && botRight == LOW && bottom == HIGH && left == HIGH){
    if(standby == true || turnRightS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = true;
      reverseS = false;
      forwardS = false;
     turnRight();
   Serial.print("IR Right\n");
    }
    else{
     movement_stop();
    standby = false;
    forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   }
   
  
  }
  
  else if(front == HIGH && topRight == HIGH && botRight == LOW && bottom == LOW && left == HIGH){ //reverse
    if(standby == true || reverseS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = false;
      reverseS = true;
      forwardS = false;
      moveReverse();
   Serial.print("IR Reverse\n");
    }
    else{
     movement_stop();
     Serial.print("IR Stop\n");
     standby = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   }
  }
  
  else if(front == HIGH && topRight == HIGH && botRight == HIGH && bottom == LOW && left == LOW){
    if(standby == true || turnLeftS == true){
      standby = false;
      turnLeftS = true;
      turnRightS = false;
      reverseS = false;
      forwardS = false;
     turnLeft();
    Serial.print("IR Left\n");
    }
    else{
     movement_stop();
     standby = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   }
  }
}

  
  int changeIR(int front, int topRight, int botRight, int bottom, int left){
    if(standby1 == false && standby2 == false){
      if(front == HIGH && topRight == LOW && botRight == LOW && bottom == LOW && left == HIGH){
        standby1 = true; 
        standby2 = true;
        Serial.print("standby1\n");
      }
    }
    else if(standby1 == true && standby2 == true){
      if(front == HIGH && topRight == LOW && botRight == LOW && bottom == LOW && left == HIGH){
        Serial.print("Standby2\n");
      }
      
      else{
        standby2 = false;
      }
    }
    else if(standby1 == true && standby2 == false && delayStart == false){
      delayStart  = true;
      startTime = millis();
      
    }
    else if(standby1 == true && standby2 == false && delayStart == true){
     
     currentTime = millis();
     
       if(currentTime - startTime > 1000){
       Serial.print("Read inputs\n");
       if (set == false){
       standby = true;
       set = true;
       }
     
    
  
  
  if(front == LOW && topRight == LOW && botRight == LOW && bottom == HIGH && left == HIGH){
    if(standby == true || forwardS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = false;
      reverseS = false;
      forwardS = true;
     moveForward();
   Serial.print("IR Forward\n");
    }
    else{
     movement_stop();
     standby = false;
     standby1=false;
     delayStart = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   set = false;
   }
  }
  
  else if(front == HIGH && topRight == LOW && botRight == LOW && bottom == HIGH && left == HIGH){
    if(standby == true || turnRightS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = true;
      reverseS = false;
      forwardS = false;
     turnRight();
   Serial.print("IR Right\n");
    }
    else{
     movement_stop();
     standby = false;
     standby1=false;
     delayStart = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   set = false;
   }
   
  
  }
  
  else if(front == HIGH && topRight == HIGH && botRight == LOW && bottom == LOW && left == HIGH){ //reverse
    if(standby == true || reverseS == true){
      standby = false;
      turnLeftS = false;
      turnRightS = false;
      reverseS = true;
      forwardS = false;
      moveReverse();
   Serial.print("IR Reverse\n");
    }
    else{
     movement_stop();
     Serial.print("IR Stop\n");
     standby = false;
     standby1=false;
     delayStart = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   set = false;
   }
  }
  
  else if(front == HIGH && topRight == HIGH && botRight == HIGH && bottom == LOW && left == LOW){
    if(standby == true || turnLeftS == true){
      standby = false;
      turnLeftS = true;
      turnRightS = false;
      reverseS = false;
      forwardS = false;
     turnLeft();
    Serial.print("IR Left\n");
    }
    else{
     movement_stop();
     standby = false;
     standby1=false;
     delayStart = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   set = false;
   }
  }
   
   else{
     movement_stop();
     Serial.print("Check\n");
     standby = false;
     standby1=false;
     delayStart = false;
     forwardS = false; 
   turnRightS = false;
   turnLeftS = false;
   reverseS = false;
   set = false;
   }
   
  }
}
  }


void checkSensors(boolean disabled){
  if(disabled == false){
    digitalWrite(LED_sensors, LOW);
    cycleSensors();
  }
  else{
    digitalWrite(LED_sensors, HIGH);
  }
}





